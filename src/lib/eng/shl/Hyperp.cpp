// ---------------------------------------------------------------------------
// - Hyperp.cpp                                                              -
// - afnix engine - hyper interpreter class implementation                   -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Vector.hpp"
#include "Interp.hpp"
#include "Hyperp.hpp"
#include "Boolean.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------
  
  // create an hyperblock by grid
  static Vector* grid_to_hblk (const Grid& grid, Vector* argv) {
    // get the grid dimensions
    long dims = grid.getdims ();
    if (dims <= 0) {
      throw Exception ("hyperp-error", "invlid grid dimensions");
    }
    // create the hyper block by dimensions
    Vector* hblk = new Vector (dims);
    // create the master interpreter at index 0
    Interp* interp = new Interp (false);
    hblk->add (interp);
    // update the argument vector
    if (argv != nullptr) interp->getargv()->merge (*argv);
    // add the other interpreters by cloning
    for (long k = 1L; k < dims; k++) hblk->add (interp->clone ());
    // here it is
    return hblk;
  }
  
  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default hyper interpreter

  Hyperp::Hyperp (void) {
    p_hblk = nullptr;
    Object::iref (p_argv = new Vector);
  }

  // destroy this hyper interpreter

  Hyperp::~Hyperp (void) {
    Object::dref (p_hblk);
    Object::dref (p_argv);
  }
  
  // return the class name

  String Hyperp::repr (void) const {
    return "Hyperp";
  }

  // get the hyper interpreter grid

  Grid Hyperp::getgrid (void) const {
    rdlock ();
    try {
      Grid result = d_grid;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the hyper interpreter arguments

  Vector* Hyperp::getargv (void) const {
    rdlock ();
    try {
      Vector* result = p_argv;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // load a module in the hyper interpreter

  bool Hyperp::load (Module& mp) {
    wrlock ();
    try {
      // check for an hyper block
      if (p_hblk == nullptr) {
	Object::iref (p_hblk = grid_to_hblk (d_grid, p_argv));
      }
      // execute the module
      long hlen = p_hblk->length ();
      if (hlen > 1L) {
	throw Exception ("hyperp-error", "unimplemented grid module load");
      }
      // get the single interpreter
      auto interp = dynamic_cast<Interp*>(p_hblk->get(0));
      // execute the module
      bool status = interp->load (mp, false);
      unlock ();
      return status;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 2;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_LOAD    = zone.intern ("load");
  static const long QUARK_GETGRID = zone.intern ("get-grid");
  static const long QUARK_GETARGV = zone.intern ("get-arguments");

  // create a new object in a generic way

  Object* Hyperp::mknew (Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();
    // check for 0 argument
    if (argc == 0L) return new Hyperp;
    // invalid arguments
    throw Exception ("argument-error", 
		     "invalid arguments with with file information");
  }

  // return true if the given quark is defined

  bool Hyperp::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
        unlock ();
        return true;
      }
      // check the object class
      bool result = hflg ? Object::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments and a quark
  
  Object* Hyperp::apply (Evaluable* zobj, Nameset* nset, const long quark,
			 Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();

    // dispatch 0 argument
    if (argc == 0L) {
      if (quark == QUARK_GETGRID) return new Grid (getgrid ());
      if (quark == QUARK_GETARGV) {
	rdlock ();
	try {
	  Object* result = getargv ();
	  zobj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
    }
    // dispatch 1 argument
    if (argc == 1L) {
      // collect object
      Object*  obj = argv->get (0);
      // check for a module
      auto* mp = dynamic_cast <Module*> (obj);
      if (mp != nullptr) return new Boolean (load (*mp));
      // invalid object
      throw Exception ("type-error", "invalid object with load",
		       Object::repr (obj));
    }
    // call the object method
    return Object::apply (zobj, nset, quark, argv);
  }
}
