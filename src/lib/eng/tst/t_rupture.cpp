// ---------------------------------------------------------------------------
// - t_rupture.cpp                                                           -
// - afnix engine - rupture object tester module                             -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Rupture.hpp"

int main (int, char**) {
  using namespace afnix;

  // create a default return
  Return retn;
  if (retn.repr () != "Return") return 1;
  // check the object
  if (retn.getrobj () != nullptr) return 1;

  // create a default break
  Break brkn;
  if (brkn.repr () != "Break") return 1;
  // check the object
  if (brkn.getrobj () != nullptr) return 1;

  // create a default continue
  Continue cotn;
  if (cotn.repr () != "Continue") return 1;
  // check the object
  if (cotn.getrobj () != nullptr) return 1;

  
  // success
  return 0;
}
