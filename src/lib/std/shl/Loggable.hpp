// ---------------------------------------------------------------------------
// - Loggable.hpp                                                            -
// - standard object library - loggable class definition                     -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_LOGGABLE_HPP
#define  AFNIX_LOGGABLE_HPP

#ifndef  AFNIX_LOGGER_HPP
#include "Logger.hpp"
#endif

namespace afnix {

  /// The Loggable class is a base class designed for loggable object. The
  /// class provides useful methods for message logging once a logger has
  /// bound to the object.
  /// @author amaury darsch

  class Loggable : public virtual Object {
  protected:
    /// the logger object
    Logger* p_logr;
    
  public:
    /// create a default loggable
    Loggable (void);

    /// create a loggable by logger
    /// @param logr the logger object
    Loggable (Logger* logr);
    
    /// copy construct this loggable object
    /// @param that the loggable to copy
    Loggable (const Loggable& that);

    /// copy move this loggable object
    /// @param that the loggable to move
    Loggable (Loggable&& that) noexcept;

    /// destroy this loggable object
    ~Loggable (void);
    
    /// assign a loggable into this one
    /// @param that the loggable to move
    Loggable& operator = (const Loggable& that);

    /// move a loggable into this one
    /// @param that the loggable to move
    Loggable& operator = (Loggable&& that) noexcept;

    /// @return the class name
    String repr (void) const override;

    /// @return a clone of this object
    Object* clone (void) const override;
    
    /// bind a logger to this object
    /// @param logr the logger to bind
    virtual bool bind (Logger* logr);

    /// log an info message
    /// @param mesg the message to log
    virtual void mvlog (const String& mesg);

    /// log an error message
    /// @param mesg the message to log
    virtual void evlog (const String& mesg);

    /// log a debug message
    /// @param mesg the message to log
    virtual void dvlog (const String& mesg);

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
	/// @param quark the quark to apply these arguments
	/// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
		   Vector* argv) override;
  };
}

#endif
