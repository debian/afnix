# ---------------------------------------------------------------------------
# - NWG0015.als                                                             -
# - afnix:nwg module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   mime test unit
# @author amaury darsch

# get the module
interp:library "afnix-nwg"

# check the iso 3166 predicates
assert true  (afnix:nwg:iso-3166-p "France")
assert true  (afnix:nwg:iso-cca2-p "FR")
assert true  (afnix:nwg:iso-cca3-p "FRA")
assert true  (afnix:nwg:iso-cnum-p 250)

# check iso 3166 name
assert "FR"  (afnix:nwg:iso-to-cca2 "France")
assert "FRA" (afnix:nwg:iso-to-cca3 "France")
assert 250   (afnix:nwg:iso-to-cnum "France")
