// ---------------------------------------------------------------------------
// - Iso.hpp                                                                -
// - afnix:nwg module - iso base class definition                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_ISO_HPP
#define  AFNIX_ISO_HPP

#ifndef  AFNIX_STRING_HPP
#include "String.hpp"
#endif

namespace afnix {

  /// The Iso class is class that provides convenient functions related
  /// to various iso standards. In particular, the class defines the
  /// iso 3166, that is country codes and numbers.
  /// @author amaury darsch

  class Iso {
  public:
    /// @return true if a country exists
    static bool is3166 (const String& name);

    /// @return true if a country cca2 exists
    static bool iscca2 (const String& cca2);

    /// @return true if a country cca3 exists
    static bool iscca3 (const String& cca3);

    /// @return true if a country number exists
    static bool iscnum (const long cnum);
    
    /// @return a country alpha-2 code by name
    static String tocca2 (const String& name);

    /// @return a country alpha-3 code by name
    static String tocca3 (const String& name);

    /// @return a country number by name
    static long tocnum (const String& name);
  };
}

#endif
