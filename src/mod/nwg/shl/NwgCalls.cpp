// ---------------------------------------------------------------------------
// - NwgCalls.cpp                                                            -
// - afnix:nwg module - specific calls implementation                        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Uri.hpp"
#include "Iso.hpp"
#include "Cons.hpp"
#include "Mime.hpp"
#include "Vector.hpp"
#include "Boolean.hpp"
#include "Integer.hpp"
#include "NwgCalls.hpp"
#include "Exception.hpp"
#include "transient.tcc"

namespace afnix {

  // -------------------------------------------------------------------------
  // - public section                                                        -
  // -------------------------------------------------------------------------

  // check if a mime extension is defined

  Object* nwg_mextp (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // get the mime extension
    if (argc == 1) {
      String mext = argv->getstring (0);
      return new Boolean (Mime::ismext (mext));
    }
    throw Exception ("argument-error",
		     "too many arguments with mime-extension-p");
  }

  // check if a mime value is defined

  Object* nwg_mvalp (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // get the mime value
    if (argc == 1) {
      String mval = argv->getstring (0);
      return new Boolean (Mime::ismval (mval));
    }
    throw Exception ("argument-error",  "too many arguments with mime-value-p");
  }

  // check for a valid xml mime value

  Object* nwg_xmlmp (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // get the mime xml value
    if (argc == 1) {
      String mval = argv->getstring (0);
      return new Boolean (Mime::isxmlm (mval));
    }
    throw Exception ("argument-error", "too many arguments with mime-xml-p");
  }

  // get a mime value by extension

  Object* nwg_tomime (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // get the mime value
    if (argc == 1) {
      String mext = argv->getstring (0);
      return new String (Mime::tomime (mext, false));
    }
    if (argc == 2) {
      String mext = argv->getstring (0);
      bool   dflg = argv->getbool   (1);
      return new String (Mime::tomime (mext, dflg));
    }
    throw Exception ("argument-error",
		     "too many arguments with extension-to-mime");
  }
  
  // get an extension by mime value

  Object* nwg_tomext (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // get the extension value
    if (argc == 1) {
      String mime = argv->getstring (0);
      return new String (Mime::tomext (mime));
    }
    throw Exception ("argument-error",
		     "too many arguments with extension-to-mime");
  }
  
  // percent encode a string

  Object* nwg_pencd (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // percent encode
    if (argc == 1) {
      String sval = argv->getstring (0);
      return new String (Uri::pencode (sval));
    }
    throw Exception ("argument-error",
		     "too many arguments with percent-encode");
  }
  
  // uri encode a string

  Object* nwg_uencd (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // uri encode
    if (argc == 1) {
      String sval = argv->getstring (0);
      return new String (Uri::uencode (sval));
    }
    throw Exception ("argument-error",  "too many arguments with uri-encode");
  }
  
  // component encode a string

  Object* nwg_cencd (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // component encode
    if (argc == 1) {
      String sval = argv->getstring (0);
      return new String (Uri::cencode (sval));
    }
    throw Exception ("argument-error",
		     "too many arguments with component-encode");
  }

  // www form encode a string

  Object* nwg_wencd (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // form encode
    if (argc == 1) {
      String sval = argv->getstring (0);
      return new String (Uri::wencode (sval));
    }
    throw Exception ("argument-error", 
		     "too many arguments with www-form-encode");
  }
  
  // percent decode a string

  Object* nwg_pdecd (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // percent decode
    if (argc == 1) {
      String sval = argv->getstring (0);
      return new String (Uri::pdecode (sval));
    }
    throw Exception ("argument-error", 
		     "too many arguments with percent-decode");
  }

  // www form decode a string

  Object* nwg_wdecd (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // form decode
    if (argc == 1) {
      String sval = argv->getstring (0);
      return new String (Uri::wdecode (sval));
    }
    throw Exception ("argument-error", 
		     "too many arguments with www-form-decode");
  }
  
  // check for a normal uri string

  Object* nwg_surip (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // check uri string
    if (argc == 1) {
      String suri = argv->getstring (0);
      return new Boolean (Uri::isuri (suri));
    }
    throw Exception ("argument-error", 
		     "too many arguments with string-uri-p");
  }
  
  // create a string uri by scheme, host and port

  Object* nwg_tosuri (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // create uri string
    if (argc == 2) {
      String schm = argv->getstring (0);
      String host = argv->getstring (1);
      return new String (Uri::tosuri (schm, host, 0));
    }
    if (argc == 3) {
      String schm = argv->getstring (0);
      String host = argv->getstring (1);
      long   port = argv->getlong   (2);
      return new String (Uri::tosuri (schm, host, port));
    }
    throw Exception ("argument-error", 
		     "too many arguments with to-string-uri");
  }
  
  // normalize a uri name by adding a scheme if any

  Object* nwg_nrmunm (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // normalize uri string
    if (argc == 1) {
      String name = argv->getstring (0);
      return new String (Uri::nrmname (name));
    }
    if (argc == 2) {
      String name = argv->getstring (0);
      bool   flag = argv->getbool   (1);
      Uri uri = Uri::nrmname (name);
      if (flag == true) uri.nrmauth ();
      return new String (uri.getanam ());
    }
    throw Exception ("argument-error", 
		     "too many arguments with normalize-uri-name");
  }

  // normalize a uri name by prioritizing the system path

  Object* nwg_sysunm (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // normalize uri name
    if (argc == 1) {
      String path = argv->getstring (0);
      return new String (Uri::sysname (path));
    }
    throw Exception ("argument-error", 
		     "too many arguments with system-uri-name");
  }
  
  // normalize a path by uri name
  
  Object* nwg_pthunm (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // normalize uri path
    if (argc == 1) {
      String path = argv->getstring (0);
      return new String (Uri::pthname (path));
    }
    throw Exception ("argument-error", 
		     "too many arguments with path-uri-name");
  }

  // normalize a host by authority

  Object* nwg_nrmhost (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // normalize host
    if (argc == 1) {
      String auth = argv->getstring (0);
      return new String (Uri::nrmhost (auth));
    }
    throw Exception ("argument-error", 
		     "too many arguments with normalize-uri-host");
  }
  
  // normalize a port by authority

  Object* nwg_nrmport (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // mormalize port
    if (argc == 1) {
      String auth = argv->getstring (0);
      return new Integer (Uri::nrmport (auth));
    }
    throw Exception ("argument-error", 
		     "too many arguments with normalize-uri-port");
  }

  // check if an iso 3166 name exists

  Object* nwg_is3166 (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // mormalize port
    if (argc == 1) {
      String name = argv->getstring (0);
      return new Boolean (Iso::is3166 (name));
    }
    throw Exception ("argument-error", 
		     "too many arguments with iso-3166-p");
  }

  // check if an iso 3166 alpha-2 exists

  Object* nwg_iscca2 (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // mormalize port
    if (argc == 1) {
      String cca2 = argv->getstring (0);
      return new Boolean (Iso::iscca2 (cca2));
    }
    throw Exception ("argument-error", 
		     "too many arguments with iso-cca2-p");
  }

  // check if an iso 3166 alpha-3 exists
  
  Object* nwg_iscca3 (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // mormalize port
    if (argc == 1) {
      String cca3 = argv->getstring (0);
      return new Boolean (Iso::iscca3 (cca3));
    }
    throw Exception ("argument-error", 
		     "too many arguments with iso-cca3-p");
  }

  // check if an iso 3166 numberexists
  
  Object* nwg_iscnum (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // mormalize port
    if (argc == 1) {
      long cnum = argv->getlong (0);
      return new Boolean (Iso::iscnum (cnum));
    }
    throw Exception ("argument-error", 
		     "too many arguments with iso-cnum-p");
  }

  // get an iso 3166 alpha-2 by name
  
  Object* nwg_tocca2 (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // mormalize port
    if (argc == 1) {
      String name = argv->getstring (0);
      return new String (Iso::tocca2 (name));
    }
    throw Exception ("argument-error", 
		     "too many arguments with iso-to-cca2");
  }

  // get an iso 3166 alpha-3 by name
  
  Object* nwg_tocca3 (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // mormalize port
    if (argc == 1) {
      String name = argv->getstring (0);
      return new String (Iso::tocca3 (name));
    }
    throw Exception ("argument-error", 
		     "too many arguments with iso-to-cca3");
  }
  
  // get an iso 3166 number by name
  
  Object* nwg_tocnum (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = argv.valid () ? argv->length () : 0L;
    // mormalize port
    if (argc == 1) {
      String name = argv->getstring (0);
      return new Integer (Iso::tocnum (name));
    }
    throw Exception ("argument-error", 
		     "too many arguments with iso-to-cnum");
  }
}
