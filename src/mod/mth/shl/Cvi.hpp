// ---------------------------------------------------------------------------
// - Cvi.hpp                                                                 -
// - afnix:mth module - complex vector interface definitions                 -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_CVI_HPP
#define  AFNIX_CVI_HPP

#ifndef  AFNIX_AVI_HPP
#include "Avi.hpp"
#endif

#ifndef  AFNIX_CPI_HPP
#include "Cpi.hpp"
#endif

#ifndef  AFNIX_COMPLEX_HPP
#include "Complex.hpp"
#endif

namespace afnix {

  /// This Cvi class is an abstract class that models the behavior of a
  /// complex based vector. The class extends the abstract vector interface
  /// with specific complex methods.
  /// @author amaury darsch

  class Cvi : public Avi {
  public:
    /// create a null vector
    Cvi (void) =default;

    /// create a vector by size
    /// @param size the vector size
    Cvi (const t_long size);

    /// copy construct this vector
    /// @param that the vector to copy
    Cvi (const Cvi& that);

    /// copy move this vector
    /// @param that the vector to move
    Cvi (Cvi&& that) noexcept;

    /// assign a vector into this one
    /// @param that the vector to assign
    Cvi& operator = (const Cvi& that);

    /// move a vector into this one
    /// @param that the vector to move
    Cvi& operator = (Cvi&& that) noexcept;

    /// serialize this object
    /// @param os the output stream
    void wrstream (OutputStream& os) const override;

    /// deserialize this object
    /// @param is the input stream
    void rdstream (InputStream& os) override;

    /// reset this vector
    void reset (void) override;

    /// clear this vector
    void clear (void) override;

    /// @return true if the vector is nil
    bool isnil (void) const override;

    /// @return a vector of literals
    Vector tovector (void) const override;

    /// @return the vector format
    String tofrmt (void) const override;

    /// compare two vectors
    /// @param  x the vector argument
    /// @return true if they are equals
    virtual bool operator == (const Cvi& x) const;

    /// compare two vectors
    /// @param  x the vector argument
    /// @return true if they are not equals
    virtual bool operator != (const Cvi& x) const;

    /// add a vector by a scalar
    /// @param z the scalar value
    virtual Cvi& operator += (const Complex& z);

    /// add a vector with a vector
    /// @param x the vector argument
    virtual Cvi& operator += (const Cvi& x);

    /// substract a vector by a scalar
    /// @param z the scalar value
    virtual Cvi& operator -= (const Complex& z);

    /// multiply a vector by a scalar
    /// @param z the scalar value
    virtual Cvi& operator *= (const Complex& z);

    /// compute the vector dot product
    /// @param x the vector argument
    virtual Complex operator ^ (const Cvi& x) const;

    /// copy a vector into this one
    /// @param x the vector to copy
    virtual Cvi& cpy (const Cvi& x);

    /// compare a vector value
    /// @param pos the vector position
    /// @param val the value to compare
    virtual bool cmp (const t_long pos, const Complex& val) const;

    /// compare two vectors upto a precision
    /// @param x the vector argument
    virtual bool cmp (const Cvi& x) const;

    /// @return the vector norm
    virtual t_real norm (void) const;

    /// set a vector by value
    /// @param val the value to set
    virtual void set (const Complex& val);

    /// set a vector by position
    /// @param pos the vector position
    /// @param val the value to set
    virtual void set (const t_long pos, const Complex& val);

    /// get a vector value by position
    /// @param pos the vector position
    virtual Complex get (const t_long pos) const;

    /// add a vector with a scalar
    /// @param x the vector argument
    /// @param z the scalar factor
    virtual Cvi& add (const Cvi& x, const Complex& z);

    /// add a vector with another one
    /// @param x the vector argument
    /// @param y the vector argument
    virtual Cvi& add (const Cvi& x, const Cvi& y);

    /// add a vector with another scaled one
    /// @param x the vector argument
    /// @param y the vector argument
    /// @param z the scalar factor
    virtual Cvi& add (const Cvi& x, const Cvi& y, const Complex& z);

    /// substract a vector with a scalar
    /// @param x the vector argument
    /// @param z the scalar factor
    virtual Cvi& sub (const Cvi& x, const Complex& z);

    /// substract a vector with another one
    /// @param x the vector argument
    /// @param y the vector argument
    virtual Cvi& sub (const Cvi& x, const Cvi& y);

    /// multiply a vector with a scaled vector
    /// @param x the vector to multiply
    /// @param z the scaling factor
    virtual Cvi& mul (const Cvi& x, const Complex& z);

    /// multiply a vector with another one
    /// @param x the vector argument
    /// @param y the vector argument
    virtual Cvi& mul (const Cvi& x, const Cvi& y);

    /// divide a vector with another one
    /// @param x the vector argument
    /// @param y the vector argument
    virtual Cvi& div (const Cvi& x, const Cvi& y);

    /// add equal with a vector
    /// @param x the vector to add
    virtual Cvi& aeq (const Cvi& x);

    /// add equal with a scaled vector
    /// @param x the vector to add
    /// @param z the scaling factor
    virtual Cvi& aeq (const Cvi& x, const Complex& z);

    /// rescale equal with a vector
    /// @param x the vector to add
    /// @param z the scaling factor
    virtual Cvi& req (const Cvi& x, const Complex& s);

    /// normalize this vector
    virtual Cvi& normalize (void);

    /// permutate this vector
    /// @param p the permutation object
    virtual Cvi* permutate (const Cpi& p) const;

    /// reverse this vector permutation
    /// @param p the permutation object
    virtual Cvi* reverse (const Cpi& p) const;

  public:
    /// no lock - set a vector by position
    /// @param pos the vector position
    /// @param val the value to set
    virtual void nlset (const t_long pos, const Complex& val) =0;

    /// no lock - get a vector value by position
    /// @param pos the vector position
    virtual Complex nlget (const t_long pos) const =0;

  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// operate this object with another object
    /// @param type   the operator type
    /// @param object the operand object
    Object* oper (t_oper type, Object* object) override;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
                   Vector* argv) override;
  };
}

#endif
