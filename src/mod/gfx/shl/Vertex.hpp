// ---------------------------------------------------------------------------
// - Vertex.hpp                                                              -
// - afnix:gfx module - graph vertex class definition                        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_VERTEX_HPP
#define  AFNIX_VERTEX_HPP

#ifndef  AFNIX_SET_HPP
#include "Set.hpp"
#endif

#ifndef  AFNIX_STATE_HPP
#include "State.hpp"
#endif

#ifndef  AFNIX_COLLECTABLE_HPP
#include "Collectable.hpp"
#endif

namespace afnix {

  /// The Vertex class is the class used to represent an vertex in the standard
  /// graph. The vertex holds two arrays for the incoming and outgoing edges.
  /// @author amaury darsch

  class Vertex : public State, public Collectable {
  protected:
    /// the edge set
    Set d_eset;

  public:
    /// create an empty vertex
    Vertex (void) =default;

    /// create a vertex with a client object
    /// @param cobj the client object
    Vertex (Object* cobj);

    /// destroy this vertex
    ~Vertex (void);
    
    /// @return the class name
    String repr (void) const override;

    /// release this vertex
    void release (void) override;

    /// @return the degree of this vertex
    virtual long degree (void) const;

    /// add an edge to this vertex
    /// @param edge the edge to add
    virtual void add (class Edge* edge);

    /// @return an edge by index
    virtual class Edge* get (const long index) const;

  private:
    // make the copy constructor private
    Vertex (const Vertex&) =delete;
    // make the assignment operator private
    Vertex& operator = (const Vertex&) =delete;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
		   Vector* argv) override;
  };
}

#endif
