// ---------------------------------------------------------------------------
// - Global.hpp                                                              -
// - afnix:gfx module - automaton global class definition                    -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_GLOBAL_HPP
#define  AFNIX_GLOBAL_HPP

#ifndef  AFNIX_PLIST_HPP
#include "Plist.hpp"
#endif

namespace afnix {

  /// The Global class is an automaton global context which is used
  /// by the transition object to compute the next state from a current state.
  /// @author amaury darsch

  class Global : public Plist {
  public:
    /// create a default global
    Global (void) =default;

    /// copy construct this global
    /// @param that the object copy
    Global (const Global& that);

    /// assign a global to this one
    /// @param that the object to assign
    Global& operator = (const Global& that);

    /// @return the class name
    String repr (void) const override;

    /// @return a clone of this object
    Object* clone (void) const override;
    
    /// @return the serial did
    t_word getdid (void) const override;

    /// @return the serial sid
    t_word getsid (void) const override;
    
    /// serialize this uuid
    /// @param os the output stream
    void wrstream (OutputStream& os) const override;

    /// deserialize this uuid
    /// @param is the input stream
    void rdstream (InputStream& os) override;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
		   Vector* argv) override;
  };
}

#endif
