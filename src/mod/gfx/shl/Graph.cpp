// ---------------------------------------------------------------------------
// - Graph.cpp                                                               -
// - afnix:gfx module - graph base class implementation                      -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Graph.hpp"
#include "Vector.hpp"
#include "Integer.hpp"
#include "Boolean.hpp"
#include "Evaluable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // return the graph class name

  String Graph::repr (void) const {
    return "Graph";
  }

  // reset the graph

  void Graph::reset (void) {
    wrlock ();
    try {
      d_vset.reset ();
      d_eset.reset ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // clear the graph

  void Graph::clear (void) {
    wrlock ();
    try {
      // clear all vertices
      for (auto vobj : d_vset) {
	auto vrtx = dynamic_cast<Vertex*>(vobj);
	if (vrtx != nullptr) vrtx->clear ();
      }
      // clear all edges
      for (auto eobj : d_eset) {
	auto edge = dynamic_cast<Edge*>(eobj);
	if (edge != nullptr) edge->clear ();
      }
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // return true if a vertex exists in this graph
  
  bool Graph::isvrtx (const long sidx) const {
    rdlock ();
    try {
      bool result = false;
      // loop in all vertices
      long nv = d_vset.length ();
      for (long k = 0L; k < nv; k++) {
	auto vrtx = dynamic_cast<Vertex*>(d_vset.get (k));
	if (vrtx == nullptr) continue;
	if (vrtx->getsidx () != sidx) continue;
	result = true; break;
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // return true if an edge exists in this graph
  
  bool Graph::isedge (const long sidx) const {
    rdlock ();
    try {
      bool result = false;
      // loop in all edges
      long ne = d_eset.length ();
      for (long k = 0L; k < ne; k++) {
	auto edge = dynamic_cast<Edge*>(d_eset.get (k));
	if (edge == nullptr) continue;
	if (edge->getsidx () != sidx) continue;
	result = true; break;
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
 
  // add a vertex to this graph

  void Graph::add (Vertex* vrtx) {
    wrlock ();
    try {
      if (vrtx == nullptr) {
	unlock ();
	return;
      }
      if (vrtx->getsidx () == -1L) {
	// update the index
	vrtx->setsidx (d_vset.length ());
	// add the vertex
	d_vset.add (vrtx);
	// collect all edges and add them
	long ne = vrtx->degree ();
	for (long k = 0L; k < ne; k++) add (vrtx->get (k));
      }
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add an edge to this graph

  void Graph::add (Edge* edge) {
    wrlock ();
    try {
      // check for nil
      if (edge == nullptr) {
	unlock ();
	return;
      }
      if (edge->getsidx () == -1L) {
	// update the index
	edge->setsidx (d_eset.length ());
	// add the edge
	d_eset.add (edge);
	// collect all vertices and add them
	long nv = edge->cardinality ();
	for (long k = 0L; k < nv; k++) add (edge->get (k));
      }
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // return the number of vertices

  long Graph::getvnum (void) const {
    rdlock ();
    try {
      long result = d_vset.length ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // return the number of edges

  long Graph::getenum (void) const {
    rdlock ();
    try {
      long result = d_eset.length ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get a vertex by index

  Vertex* Graph::getvrtx (const long vidx) const {
    rdlock ();
    try {
      auto result = dynamic_cast <Vertex*> (d_vset.get (vidx));
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get an edge by index

  Edge* Graph::getedge (const long eidx) const {
    rdlock ();
    try {
      auto result = dynamic_cast <Edge*> (d_eset.get (eidx));
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 9;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the graph supported quarks
  static const long QUARK_ADD     = zone.intern ("add");
  static const long QUARK_RESET   = zone.intern ("reset");
  static const long QUARK_CLEAR   = zone.intern ("clear");
  static const long QUARK_NEDGES  = zone.intern ("number-of-edges");
  static const long QUARK_NVRTXS  = zone.intern ("number-of-vertices");
  static const long QUARK_ISEDGEP = zone.intern ("edge-p");
  static const long QUARK_ISVRTXP = zone.intern ("vertex-p");
  static const long QUARK_GETEDGE = zone.intern ("get-edge");
  static const long QUARK_GETVRTX = zone.intern ("get-vertex");

  // create a new object in a generic way

  Object* Graph::mknew (Vector* argv) {
    long argc = (argv == nullptr) ? 0 : argv->length ();

    // check for 0 argument
    if (argc == 0) return new Graph;
    // invalid arguments
    throw Exception ("argument-error", "too many arguments with graph");
  }

  // return true if the given quark is defined

  bool Graph::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Object::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // apply this object with a set of arguments and a quark
  
  Object* Graph::apply (Evaluable* zobj, Nameset* nset, const long quark,
			Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_NEDGES) return new Integer (getenum ());
      if (quark == QUARK_NVRTXS) return new Integer (getvnum ());
      if (quark == QUARK_RESET) {
	reset ();
	return nullptr;
      }
      if (quark == QUARK_CLEAR) {
	clear ();
	return nullptr;
      }
    }

    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_ISEDGEP) {
	long sidx = argv->getlong (0);
	return new Boolean (isedge (sidx));
      }
      if (quark == QUARK_ISVRTXP) {
	long sidx = argv->getlong (0);
	return new Boolean (isvrtx (sidx));
      }
      if (quark == QUARK_ADD) {
	// collect object
	Object* obj = argv->get (0);
	// check for edge
	auto edge = dynamic_cast <Edge*> (obj);
	if (edge != nullptr) {
	  add (edge);
	  return edge;
	}
	// check for vertex
	auto vrtx = dynamic_cast <Vertex*> (obj);
	if (vrtx != nullptr) {
	  add (vrtx);
	  return vrtx;
	}
	// invalid object
	throw Exception ("type-error", "invalid object to add to graph",
			 Object::repr (obj));
      }
      if (quark == QUARK_GETEDGE) {
	rdlock ();
	try {
	  long eidx = argv->getlong (0);
	  Object* result = getedge (eidx);
	  zobj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
      if (quark == QUARK_GETVRTX) {
	rdlock ();
	try {
	  long vidx = argv->getlong (0);
	  Object* result = getvrtx (vidx);
	  zobj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
    }
    // call the object method
    return Object::apply (zobj, nset, quark, argv);
  }
}
