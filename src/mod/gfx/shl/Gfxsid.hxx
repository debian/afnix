// ---------------------------------------------------------------------------
// - Gfxsid.hxx                                                              -
// - afnix:gfx module - serial id definition                                 -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_GFXSID_HXX
#define  AFNIX_GFXSID_HXX

#ifndef  AFNIX_CCNF_HPP
#include "ccnf.hpp"
#endif

namespace afnix {
  //                vvtt pppp pllu uuuu
  // gfx serial id [0000 0000 0010 0101][@0x0025U]
  static const t_word SRL_STTE_SID = 0x0000U; // state id
  static const t_word SRL_TRNT_SID = 0x0001U; // transition id
  static const t_word SRL_ATMT_SID = 0x0010U; // automaton id
  static const t_word SRL_GLOB_SID = 0x0011U; // global id

  // the gfx dispatch id
  static const t_word SRL_DEOD_GFX = 0x0025U;
}

#endif
