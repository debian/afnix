// ---------------------------------------------------------------------------
// - State.hpp                                                               -
// - afnix:gfx module - state class definition                               -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_STATE_HPP
#define  AFNIX_STATE_HPP

#ifndef  AFNIX_SERIAL_HPP
#include "Serial.hpp"
#endif

namespace afnix {

  /// The State class is a base class for the graph object library. The state
  /// is used to model vertex and edge and can be used seldom in structure
  /// like finite automaton.
  /// @author amaury darsch

  class State : public virtual Serial {
  protected:
    /// the state marker
    bool    d_mark;
    /// the state index
    long    d_sidx;
    /// the client object
    Object* p_cobj;

  public:
    /// create an empty state
    State (void);

    /// create a state with a client object
    /// @param cobj the client object
    State (Object* cobj);

    /// destroy this state
    ~State (void);

    /// @return the class name
    String repr (void) const override;

    /// @return the serial did
    t_word getdid (void) const override;

    /// @return the serial sid
    t_word getsid (void) const override;
    
    /// serialize this uuid
    /// @param os the output stream
    void wrstream (OutputStream& os) const override;

    /// deserialize this uuid
    /// @param is the input stream
    void rdstream (InputStream& os) override;
    
    /// clear this state
    virtual void clear (void);

    /// set the state index
    /// @param sidx the state index
    virtual void setsidx (const long sidx);
    
    /// @return the state index
    virtual long getsidx (void) const;
    
    /// set the state client object
    /// @param clo the client object
    virtual void setcobj (Object* cobj);

    /// @return the state client object
    virtual Object* getcobj (void) const;

  private:
    // make the copy constructor private
    State (const State&) =delete;
    // make the assignment operator private
    State& operator = (const State&) =delete;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
		   Vector* argv) override;
  };
}

#endif
