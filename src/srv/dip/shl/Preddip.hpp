// ---------------------------------------------------------------------------
// - Preddip.hpp                                                             -
// - afnix:dip service - predicates declaration                              -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PREDDIP_HPP
#define  AFNIX_PREDDIP_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// This file contains the predicates associated with the afnix image
  /// processing service.
  /// @author amaury darsch

  /// the pixel object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* dip_pixp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the image object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* dip_imgp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the texel object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* dip_txlp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the pixmap object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* dip_pxmp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the cubmap object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* dip_cbmp (Evaluable* zobj, Nameset* nset, Cons* args);
  
  /// the voxel object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* dip_vxlp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the mixmap object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* dip_mxmp (Evaluable* zobj, Nameset* nset, Cons* args);
}

#endif
