// ---------------------------------------------------------------------------
// - Transmutable.cpp                                                        -
// - afnix:tcz service - mixture transmutation abstract class implementation -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Vector.hpp"
#include "Boolean.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "Transmutable.hpp"

namespace afnix {
    
  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 5;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_BIND    = zone.intern ("bind");
  static const long QUARK_RESET   = zone.intern ("reset");
  static const long QUARK_STAGE   = zone.intern ("stage");
  static const long QUARK_PROCESS = zone.intern ("process");
  static const long QUARK_SETLOGR = zone.intern ("set-logger");

  // return true if the given quark is defined

  bool Transmutable::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Object::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments and a quark
  
  Object* Transmutable::apply (Evaluable* zobj, Nameset* nset, const long quark,
			     Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();

    // check for 0 argument
    if (argc == 0) {
      if (quark == QUARK_STAGE) return new Boolean (stage());
      if (quark == QUARK_PROCESS) return new Boolean (process(nullptr));
      if (quark == QUARK_RESET) {
	reset ();
	return nullptr;
      }
    }
    // check for 1 arguments
    if (argc == 1) {
      if (quark == QUARK_BIND) {
	// collect object
	Object* obj = argv->get (0);
	// check for a mixture
	auto mixt = dynamic_cast <Mixture*> (obj);
	if (mixt != nullptr) return new Boolean (bind (mixt));
	// invalid type
	throw Exception ("type-error", "invalid object for transmutable bind",
			 Object::repr (obj));
      }
      if (quark == QUARK_PROCESS) {
	// collect object
	Object* obj = argv->get (0);
	// check for a logger
	auto logr = dynamic_cast <Logger*> (obj);
        if (logr != nullptr) return new Boolean (process (logr));
	// invalid type
	throw Exception ("type-error", "invalid object as looger",
			 Object::repr (obj));
      }
    }
    // call the object method
    return Object::apply (zobj, nset, quark, argv);
  }
}
