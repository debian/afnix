// ---------------------------------------------------------------------------
// - Predtcz.hpp                                                             -
// - afnix:tcz service - predicates declaration                              -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PREDTCZ_HPP
#define  AFNIX_PREDTCZ_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// this file contains the predicates associated with the afnix:tcz
  /// standard service.
  /// @author amaury darsch

  /// the part object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_partp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the blob object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_blobp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the bloc object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_blocp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the carrier blob object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_cblbp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the delegate blob object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_dblbp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the tracker blob object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_tckrp (Evaluable* zobj, Nameset* nset, Cons* args);
  
  /// the collection object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_collp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the domain object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_domnp (Evaluable* zobj, Nameset* nset, Cons* args);
  
  /// the whois object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_woisp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the whatis object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_waisp (Evaluable* zobj, Nameset* nset, Cons* args);
  
  /// the workzone object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_wzonp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the workspace object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_wspcp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the localzone object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_lzonp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the localspace object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_lspcp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the userspace object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_uspcp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the realm object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_relmp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the realm zone object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_rzonp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the identity object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_idtyp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the authority object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_authp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the principal object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_pcplp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the act object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_xactp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the visa object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_visap (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the credential object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_credp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the notary object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_ntryp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the datum object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_dtump (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the mixture object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_mixtp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the transmutable object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_trmtp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the transmuter object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tcz_trmrp (Evaluable* zobj, Nameset* nset, Cons* args);
}

#endif
