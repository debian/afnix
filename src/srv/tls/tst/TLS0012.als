# ---------------------------------------------------------------------------
# - TLS0012.als                                                             -
# - afnix:tls module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2020 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   pkdhe test unit
# @author amaury darsch

# get the module
interp:library "afnix-sec"
interp:library "afnix-tls"

# create a pkdhe object by path (pem/der)
const ppem (afnix:tls:Pkdhe "DHE0012.pem")
const pder (afnix:tls:Pkdhe "DHE0012.der")
# check representation
assert true    (afnix:tls:pkdhe-p ppem)
assert true    (afnix:tls:pkdhe-p pder)
assert "Pkdhe" (ppem:repr)
assert "Pkdhe" (pder:repr)

# check type
assert afnix:tls:Pem:DH-PARAMETERS (ppem:get-type)
assert afnix:tls:Pem:DH-PARAMETERS (pder:get-type)

# compare keys
const kder (pder:get-key)
const kpem (ppem:get-key)
assert afnix:sec:Key:KDHE (kder:get-type)
assert afnix:sec:Key:KDHE (kpem:get-type)

assert (kder:get-size) (kpem:get-size)
assert (kder:get-bits) (kpem:get-bits)

trans rder (kder:get-relatif-key afnix:sec:Key:DH-P-PRIME)
trans rpem (kpem:get-relatif-key afnix:sec:Key:DH-P-PRIME)
assert rder rpem

trans rder (kder:get-relatif-key afnix:sec:Key:DH-GROUP-GENERATOR)
trans rpem (kpem:get-relatif-key afnix:sec:Key:DH-GROUP-GENERATOR)
assert rder rpem

trans rder (kder:get-relatif-key afnix:sec:Key:DH-GROUP-ORDER)
trans rpem (kpem:get-relatif-key afnix:sec:Key:DH-GROUP-ORDER)
assert rder rpem
