// ---------------------------------------------------------------------------
// - TlsSuite.hxx                                                            -
// - afnix:tls service - tls cipher suite internals                          -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TLSSUITE_HXX
#define  AFNIX_TLSSUITE_HXX

#ifndef  AFNIX_TLSSUITE_HPP
#include "TlsSuite.hpp"
#endif

#ifndef  AFNIX_TLSTYPES_HXX
#include "TlsTypes.hxx"
#endif

namespace afnix {
  // rfc::2246 cipher suite
  static const t_word TLS_NULL_WITH_NULL_NULL             = 0x0000U;
  static const t_word TLS_RSA_WITH_NULL_MD5               = 0x0001U;
  static const t_word TLS_RSA_WITH_NULL_SHA               = 0x0002U;
  static const t_word TLS_RSA_WITH_RC4_128_MD5            = 0x0004U;
  static const t_word TLS_RSA_WITH_RC4_128_SHA            = 0x0005U;
  // rfc::3268 cipher suite
  static const t_word TLS_RSA_WITH_AES_128_CBC_SHA        = 0x002FU;
  static const t_word TLS_RSA_WITH_AES_256_CBC_SHA        = 0x0035U;
  static const t_word TLS_DHE_RSA_WITH_AES_128_CBC_SHA    = 0x0033U;
  static const t_word TLS_DHE_RSA_WITH_AES_256_CBC_SHA    = 0x0039U;
  // rfc 5246 cipher suite
  static const t_word TLS_RSA_WITH_AES_128_CBC_SHA256     = 0x003CU;
  static const t_word TLS_RSA_WITH_AES_256_CBC_SHA256     = 0x003DU;
  static const t_word TLS_DHE_RSA_WITH_AES_128_CBC_SHA256 = 0x0067U;
  static const t_word TLS_DHE_RSA_WITH_AES_256_CBC_SHA256 = 0x006BU;
  // rfc 5288 cipher suite
  static const t_word TLS_RSA_WITH_AES_128_GCM_SHA256     = 0x009CU;
  static const t_word TLS_RSA_WITH_AES_256_GCM_SHA384     = 0x009DU;
  static const t_word TLS_DHE_RSA_WITH_AES_128_GCM_SHA256 = 0x009EU;
  static const t_word TLS_DHE_RSA_WITH_AES_256_GCM_SHA384 = 0x009FU;
  
  // the cipher list definition
  static const TlsSuite::s_cnfo TLS_CNFO_LST[] =
    {
      // 0x00, 0x00
      {
	"TLS_NULL_WITH_NULL_NULL", TLS_NULL_WITH_NULL_NULL,
	TLS_VCOD_V10, TLS_VCOD_ALL,
	TlsSuite::TLS_EXCH_NIL, Crypto::SIGN_NIL,
	Crypto::CIFR_NIL, TlsSuite::TLS_CMOD_NIL, 0L, 0L, 0L,
	Crypto::HASH_NIL, 0L, false, false
      },
      // 0x00, 0x01
      {
	"TLS_RSA_WITH_NULL_MD5", TLS_RSA_WITH_NULL_MD5,
	TLS_VCOD_V10, TLS_VCOD_ALL,
	TlsSuite::TLS_EXCH_RSA, Crypto::SIGN_NIL,
	Crypto::CIFR_NIL, TlsSuite::TLS_CMOD_NIL, 0L, 0L, 0L,
	Crypto::HASH_MD5, 16L, true, true
      },
      // 0x00, 0x02
      {
	"TLS_RSA_WITH_NULL_SHA", TLS_RSA_WITH_NULL_SHA,
	TLS_VCOD_V10, TLS_VCOD_ALL,
	TlsSuite::TLS_EXCH_RSA, Crypto::SIGN_NIL,
	Crypto::CIFR_NIL, TlsSuite::TLS_CMOD_NIL, 0L, 0L, 0L,
	Crypto::HASH_SHA1, 20L, true, true
      },
      // 0x00, 0x04
      {
	"TLS_RSA_WITH_RC4_128_MD5", TLS_RSA_WITH_RC4_128_MD5,
	TLS_VCOD_NIL, TLS_VCOD_NIL,
	TlsSuite::TLS_EXCH_RSA, Crypto::SIGN_NIL,
	Crypto::CIFR_RC4, TlsSuite::TLS_CMOD_NIL, 16L, 0L, 0L,
	Crypto::HASH_MD5, 16L, true, true
      },
      // 0x00, 0x05
      {
	"TLS_RSA_WITH_RC4_128_SHA", TLS_RSA_WITH_RC4_128_SHA,
	TLS_VCOD_NIL, TLS_VCOD_NIL,
	TlsSuite::TLS_EXCH_RSA, Crypto::SIGN_NIL,
	Crypto::CIFR_RC4, TlsSuite::TLS_CMOD_NIL, 16L, 0L, 0L,
	Crypto::HASH_SHA1, 20L, true, true
      },
      // 0x00, 0x2F
      {
	"TLS_RSA_WITH_AES_128_CBC_SHA", TLS_RSA_WITH_AES_128_CBC_SHA,
	TLS_VCOD_V10, TLS_VCOD_V10,
	TlsSuite::TLS_EXCH_RSA, Crypto::SIGN_NIL,
	Crypto::CIFR_AES, TlsSuite::TLS_CMOD_CBC, 16L, 16L, 16L,
	Crypto::HASH_SHA1, 20L, true, true
      },
      {
	"TLS_RSA_WITH_AES_128_CBC_SHA", TLS_RSA_WITH_AES_128_CBC_SHA,
	TLS_VCOD_V11, TLS_VCOD_ALL,
	TlsSuite::TLS_EXCH_RSA, Crypto::SIGN_NIL,
	Crypto::CIFR_AES, TlsSuite::TLS_CMOD_EBC, 16L, 16L, 0L,
	Crypto::HASH_SHA1, 20L, true, true
      },
      // 0x00, 0x33
      {
	"TLS_DHE_RSA_WITH_AES_128_CBC_SHA", TLS_DHE_RSA_WITH_AES_128_CBC_SHA,
	TLS_VCOD_V10, TLS_VCOD_V10,
	TlsSuite::TLS_EXCH_DHE, Crypto::SIGN_RSA,
	Crypto::CIFR_AES, TlsSuite::TLS_CMOD_CBC, 16L, 16L, 16L,
	Crypto::HASH_SHA1, 20L, true, true
      },
      {
	"TLS_DHE_RSA_WITH_AES_128_CBC_SHA", TLS_DHE_RSA_WITH_AES_128_CBC_SHA,
	TLS_VCOD_V11, TLS_VCOD_ALL,
	TlsSuite::TLS_EXCH_DHE, Crypto::SIGN_RSA,
	Crypto::CIFR_AES, TlsSuite::TLS_CMOD_EBC, 16L, 16L, 0L,
	Crypto::HASH_SHA1, 20L, true, true
      },
      // 0x00, 0x35
      {
	"TLS_RSA_WITH_AES_256_CBC_SHA", TLS_RSA_WITH_AES_256_CBC_SHA,
	TLS_VCOD_V10, TLS_VCOD_V10,
	TlsSuite::TLS_EXCH_RSA, Crypto::SIGN_NIL, 
	Crypto::CIFR_AES, TlsSuite::TLS_CMOD_CBC, 32L, 16L, 16L,
	Crypto::HASH_SHA1, 20L, true, true
      },
      {
	"TLS_RSA_WITH_AES_256_CBC_SHA", TLS_RSA_WITH_AES_256_CBC_SHA,
	TLS_VCOD_V11, TLS_VCOD_ALL,
	TlsSuite::TLS_EXCH_RSA, Crypto::SIGN_NIL, 
	Crypto::CIFR_AES, TlsSuite::TLS_CMOD_EBC, 32L, 16L, 0L,
	Crypto::HASH_SHA1, 20L, true, true
      },
      // 0x00, 0x39
      {
	"TLS_DHE_RSA_WITH_AES_256_CBC_SHA", TLS_DHE_RSA_WITH_AES_256_CBC_SHA,
	TLS_VCOD_V10, TLS_VCOD_V10,
	TlsSuite::TLS_EXCH_DHE,  Crypto::SIGN_RSA,
	Crypto::CIFR_AES, TlsSuite::TLS_CMOD_CBC, 32L, 16L, 16L,
	Crypto::HASH_SHA1, 20L, true, true
      },
      {
	"TLS_DHE_RSA_WITH_AES_256_CBC_SHA", TLS_DHE_RSA_WITH_AES_256_CBC_SHA,
	TLS_VCOD_V11, TLS_VCOD_ALL,
	TlsSuite::TLS_EXCH_DHE, Crypto::SIGN_RSA,
	Crypto::CIFR_AES, TlsSuite::TLS_CMOD_EBC, 32L, 16L, 0L,
	Crypto::HASH_SHA1, 20L, true, true
      },
      // 0x00, 0x3C
      {
	"TLS_RSA_WITH_AES_128_CBC_SHA256", TLS_RSA_WITH_AES_128_CBC_SHA256,
	TLS_VCOD_V12, TLS_VCOD_ALL,
	TlsSuite::TLS_EXCH_RSA, Crypto::SIGN_NIL, 
	Crypto::CIFR_AES, TlsSuite::TLS_CMOD_EBC, 16L, 16L, 0L,
	Crypto::HASH_SHA256, 32L, true, true
      },
      // 0x00, 0x3D
      {
	"TLS_RSA_WITH_AES_256_CBC_SHA256", TLS_RSA_WITH_AES_256_CBC_SHA256,
	TLS_VCOD_V12, TLS_VCOD_ALL,
	TlsSuite::TLS_EXCH_RSA, Crypto::SIGN_NIL,
	Crypto::CIFR_AES, TlsSuite::TLS_CMOD_EBC, 32L, 16L, 0L,
	Crypto::HASH_SHA256, 32L, true
      },
      // 0x00, 0x67
      {
	"TLS_DHE_RSA_WITH_AES_128_CBC_SHA256",
	TLS_DHE_RSA_WITH_AES_128_CBC_SHA256,
	TLS_VCOD_V12, TLS_VCOD_ALL,
	TlsSuite::TLS_EXCH_DHE, Crypto::SIGN_RSA, 
	Crypto::CIFR_AES, TlsSuite::TLS_CMOD_EBC, 16L, 16L, 0L,
	Crypto::HASH_SHA256, 32L, true, true
      },
      // 0x00, 0x6B
      {
	"TLS_DHE_RSA_WITH_AES_256_CBC_SHA256",
	TLS_DHE_RSA_WITH_AES_256_CBC_SHA256,
	TLS_VCOD_V12, TLS_VCOD_ALL,
	TlsSuite::TLS_EXCH_DHE, Crypto::SIGN_RSA,
	Crypto::CIFR_AES, TlsSuite::TLS_CMOD_EBC, 32L, 16L, 0L,
	Crypto::HASH_SHA256, 32L, true
      },
      // 0x00, 0x9C
      {
	"TLS_RSA_WITH_AES_128_GCM_SHA256", TLS_RSA_WITH_AES_128_GCM_SHA256,
	TLS_VCOD_V12, TLS_VCOD_ALL,
	TlsSuite::TLS_EXCH_RSA, Crypto::SIGN_NIL,
	Crypto::CIFR_AES, TlsSuite::TLS_CMOD_GCM, 16L, 16L, 4L,
	Crypto::HASH_SHA256, 32L, false, true
      },
      // 0x00, 0x9D
      {
	"TLS_RSA_WITH_AES_256_GCM_SHA384", TLS_RSA_WITH_AES_256_GCM_SHA384,
	TLS_VCOD_V12, TLS_VCOD_ALL,
	TlsSuite::TLS_EXCH_RSA, Crypto::SIGN_NIL,
	Crypto::CIFR_AES, TlsSuite::TLS_CMOD_GCM, 32L, 16L, 4L,
	Crypto::HASH_SHA384, 48L, false, true
      },
      // 0x00, 0x9E
      {
	"TLS_DHE_RSA_WITH_AES_128_GCM_SHA256",
	TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,
	TLS_VCOD_V12, TLS_VCOD_ALL,
	TlsSuite::TLS_EXCH_DHE, Crypto::SIGN_RSA,
	Crypto::CIFR_AES, TlsSuite::TLS_CMOD_GCM, 16L, 16L, 4L,
	Crypto::HASH_SHA256, 32L, false, true
      },
      // 0x00, 0x9F
      {
	"TLS_DHE_RSA_WITH_AES_256_GCM_SHA384",
	TLS_DHE_RSA_WITH_AES_256_GCM_SHA384,
	TLS_VCOD_V12, TLS_VCOD_ALL,
	TlsSuite::TLS_EXCH_DHE, Crypto::SIGN_RSA,
	Crypto::CIFR_AES, TlsSuite::TLS_CMOD_GCM, 32L, 16L, 4L,
	Crypto::HASH_SHA384, 48L, false, true
      }
    };
  // the cipher list size
  static const long TLS_CNFO_SIZ =
    sizeof (TLS_CNFO_LST) / sizeof (TlsSuite::s_cnfo);
}

#endif
