# ---------------------------------------------------------------------------
# - AXI0083.als                                                             -
# - afnix engine test module                                                -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   hyper interpreter test
# @author amaury darsch

# create an hyper interpreter
const hi (Hyperp)
assert true (hyperp-p hi)
assert "Hyperp" (hi:repr)

# set the hyper argument
apply (hi:get-arguments) add (2)

# create a text assertion
const text "assert 3 (+ (interp:argv:get 0) 1)"
# create a test module
trans tmod (Module (Buffer text))
# load the module
assert true (hi:load tmod)
