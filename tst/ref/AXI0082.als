# ---------------------------------------------------------------------------
# - AXI0082.als                                                             -
# - afnix engine test module                                                -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   module test
# @author amaury darsch

# create a test module
trans tmod (Module)
assert true (module-p tmod)
assert false (tmod:text-p)
assert "Module" (tmod:repr)

# create a text assertion
const text "assert 3 (+ 2 1)"
# create an input buffer
trans ibuf (Buffer text)
# create a test module
trans tmod (Module ibuf)
# load the module
assert true (interp:load tmod)
