# ----------------------------------------------------------------------------
# - std-adp-utils                                                            -
# - afnix:std:adp utility functions module                                   -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - copyright section                                                        -
# ----------------------------------------------------------------------------

# @return a formated revision string
const afnix:std:adp:get-revision-string nil {
  const rmaj AFNIX:STD:ADP:MODULE-RMAJ
  const rmin AFNIX:STD:ADP:MODULE-RMIN
  const ptch AFNIX:STD:ADP:MODULE-PTCH
  afnix:std:acl:get-revision-number rmaj rmin ptch
}

# @return the copyright message
const afnix:std:adp:get-copyright-message nil {
  # format the copyright message
  + (+ AFNIX:STD:ADP:MODULE-INFO ", ") AFNIX:STD:ADP:MODULE-COPY
}

# @return the full system version
const afnix:std:adp:get-revision-message nil {
  # get the revision info
  const mrev (afnix:std:adp:get-revision-string)
  # format the revision message
  const mesg (+ "revision " mrev)
  # add system info
  mesg:+= (+ ", afnix " interp:version) 
  mesg:+= (+ ", "       interp:os-name)
  # here it is
  eval mesg
}

# ----------------------------------------------------------------------------
# - format section                                                           -
# ----------------------------------------------------------------------------

# format a title by section numbers
# @param name the name to normalize
const afnix:std:adp:format-full-title (name args) {
  trans result ""
  for (v) (args) {
    if (result:nil-p) (result:+= (v:to-string)) {
      result:+= "."
      result:+= (v:to-string)
    }
  }
  if (result:nil-p) (result:+= name) {
    result:+= " - "
    result:+= name
  }
  eval result
}

# @return today's date
const afnix:std:adp:get-today-date nil {
  const date (afnix:sys:Date)
  date:to-date
}
